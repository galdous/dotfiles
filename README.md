# What is included #
- .bashrc
- nvim
- pandoc

# Prerequisites #

Install `stow`

# Cloning the repository #

```bash
$ mkdir ~/dotfiles
$ cd ~/dotfiles
$ git clone git@gitlab.com:galdous/dotfiles.git
```

# Installing a group of configs (called "packages" in GNU Stow) #

```bash
$ cd ~/dotfiles
$ stow <package>
```

Stow will place symlinks in $HOME with the same folder structure that they have in the dotfiles directory.
